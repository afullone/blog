---
layout: post
title:  "Style Guides"
date:   2014-07-25 17:48:02
categories: [coding]
---

<p>
	<a href="http://localhost/blog.antoniofullone.com/wp-content/uploads/2014/01/dutch.jpg">
		<figure class="img--post-default">
			<img class="" alt="" src="http://localhost/blog.antoniofullone.com/wp-content/uploads/2014/01/dutch.jpg">
			<figcaption>Where I would like to be now.</figcaption>
		</figure>
	</a>
</p>

Era ancora buio. Lontano, nell’ampia distesa nera dell’Alìa, ammiccava soltanto un lume di carbonai, e più a sinistra la stella del mattino, sopra un nuvolone basso che tagliava l’alba nel lungo altipiano del Paradiso. Per tutta la campagna diffondevasi un uggiolare lugubre di cani. E subito, dal quartiere basso, giunse il suono grave del campanone di San Giovanni che dava l’allarme anch’esso; poi la campana fessa di San Vito; l’altra della chiesa madre, più lontano; quella di Sant’Agata che parve addirittura cascar sul capo agli abitanti della piazzetta.

<p>
	<a href="http://localhost/blog.antoniofullone.com/wp-content/uploads/2013/11/best.jpg">
		<figure class="img--post-large"><img class="" alt="best" src="http://localhost/blog.antoniofullone.com/wp-content/uploads/2013/11/best.jpg"><figcaption>Where I will go one day</figcaption></figure>
		</a></p>
<blockquote><p>“Quando uno non ha niente, il meglio è di andarsene.”</p></blockquote>
<h2>This is a header h2</h2>
<h3>This is a header h2</h3>
<h4>This is a header h2</h4>
<h5>This is a header h2</h5>
<h3>This is a List.</h3>
<ul>
<ul>
<li>this is a list</li>
<li>item</li>
<li>item</li>
</ul>
</ul>
<ul>
<li>item
<ul>
<li>Sub list</li>
<li>Sub item</li>
</ul>
</li>
</ul>
<ol>
<li>&nbsp;Ordered list</li>
<li>Item</li>
<li>
	<ol>
<li>&nbsp;Sub Ordered list</li>
<li>Sub Item</li>
<li>Sub Item</li>
</ol>
</li>
</ol>
