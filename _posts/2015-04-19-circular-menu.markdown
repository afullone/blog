---
layout: post
title:  Circular Menu inspired by Path.
date:   2015-04-19 17:25:00
categories: dev
image:  "circular.png"
imageDesc: "Circular menu in CSS"
---

I am actually learning Swift. This, together with the fact that I am totally in love with Sketch App, has completely influenced my mind so I keep downloading apps from the app store and check their UI trying to learn as much as I can.

2 of the apps that I really like are [Path](https://path.com), the social app, and [Up24](https://itunes.apple.com/us/app/up-tracker-required-up-up24/id461125277?mt=8) the fitness tracker app.
Both are really well done and they share a similar menu. Inspired by those menu's I created a css version using animations. I needed javascript to toggle the classes but all the rest is handled in css.

Check it out [here](http://antoniofullone.com/circular-menu/).
And [here](http://antoniofullone.com/files/circular/)
