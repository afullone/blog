---
layout: post
title:  Not just about work.
date:   2014-11-05 21:30:00
categories: expat-life
image:  "team.png"
imageDesc: "With the team in barcelona for the final"
---

Tomorrow I am flying to Barcelona for a long weekend, I will meet some very nice friends there. We used to work together in Barcelona, back in 2008/2009 and we had a very nice time. I was *leading* that team, so I am proud we built this *lovely friendship* which is lasting for years.

This is the list of the countries represented in this meeting : 

- Italy
- Netherlands
- Belgium
- Sweden
- Peru
- Finland
- Spain
- Argentina (probably)
  

I met these amazing people because I went to live and work abroad, and this is the best part of being an **expat**, something I never regret I did. 

> The ideal place for me is the one in which it is most natural to live as a foreigner. - Italo Calvino.

